package com.statistics.components;

import com.statistics.enums.TimeStepEnum;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class LogFile {

    private static final String SEPARATOR = ";";
    private static final String ERROR_TAG  = "ERROR";

    private Path path;
    private Map<LocalDateTime, Integer> statistics;

    public LogFile(Path path) {
        this.path = path;
        this.statistics = new HashMap<>();
    }

    public Map<LocalDateTime, Integer> calculateStatistics(TimeStepEnum step) throws IOException {
        try (Stream<String> lines = Files.lines(path)) {
            lines.parallel().forEach(line -> this.lineAnalysis(line, step));
        }
        return statistics;
    }

    private void lineAnalysis(String line, TimeStepEnum step) {
        String[] splitLine = line.split(SEPARATOR);

        if (splitLine[1].equals(ERROR_TAG)) {
            LocalDateTime key = this.getKey(splitLine[0], step);
            statistics.put(key, statistics.containsKey(key) ? statistics.get(key) + 1 : 1);
        }
    }

    private LocalDateTime getKey(String str, TimeStepEnum step) {
        LocalDateTime logTime = LocalDateTime.parse(str);
        return  LocalDateTime.of(
                logTime.getYear(),
                logTime.getMonth(),
                logTime.getDayOfMonth(),
                logTime.getHour(),
                step == TimeStepEnum.MINUTE ? logTime.getMinute() : 0
        );
    }
}
